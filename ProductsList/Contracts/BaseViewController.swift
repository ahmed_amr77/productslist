//
//  BaseViewController.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/9/22.
//

import UIKit

class BaseViewController: UIViewController, BaseViewControllerProtocol {
    
    var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        spinnerInit()
    }

    func spinnerInit() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
    }
    
    func showLoading() {
        activityIndicator.startAnimating()
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    func showAlert(_ title: String, with errorMessage: String? = nil, actions: [UIAlertAction]? = nil) {
        let alertController = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        if let actions = actions {
            actions.forEach { (action) in
                alertController.addAction(action)
            }
        } else {
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
        present(alertController, animated: true, completion: nil)
    }
}
