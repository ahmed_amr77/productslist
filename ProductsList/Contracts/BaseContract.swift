//
//  BaseContract.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/8/22.
//

import UIKit

protocol BaseViewProtocol {
    func showLoading()
    func hideLoading()
}

protocol BaseViewControllerProtocol: BaseViewProtocol {
    func showAlert(_ title: String, with errorMessage: String?, actions: [UIAlertAction]?)
}
