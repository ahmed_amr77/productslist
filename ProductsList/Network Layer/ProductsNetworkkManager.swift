//
//  ProductsNetworkkManager.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import Foundation

protocol ProductsNetworkkManagerProtocol {
    func getProductsData(url: String, completion: @escaping (Result<[Product], Error>) -> Void)
    func getProductImage(url: String, completion: @escaping (Result<Data, Error>) -> Void)
}

class ProductsNetworkkManager: ProductsNetworkkManagerProtocol {
    
    var service = NetworkService.sharedInstance
    
    func getProductsData(url: String, completion: @escaping (Result<[Product], Error>) -> Void) {
        service.fetchData(with: url) { (result) in
            switch result {
            case .failure(let err):
                completion(.failure(err))
            case .success(let data):
                do {
                    let products = try JSONDecoder().decode([Product].self, from: data)
                    completion(.success(products))
                } catch let parseError {
                    completion(.failure(parseError))
                }
            }
        }
    }
    
    func getProductImage(url: String, completion: @escaping (Result<Data, Error>) -> Void) {
        //"https://www.nasa.gov/sites/default/files/saturn_collage.jpg"
        service.fetchData(with: url) { (result) in
            completion(result)
        }
    }
}
