//
//  NetworkService.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import Foundation

struct NetworkService {
    
    private let defaultSession: URLSession
    private var dataTask: URLSessionDataTask?
    
    private init() {
        defaultSession = URLSession(configuration: .default)
    }
    
    static let sharedInstance = NetworkService()
    
    
    mutating func fetchData(with url: String, completion: @escaping (Result<Data,Error>) -> Void) {
        
        dataTask?.cancel()
        
        if let url = URL(string: url) {
            dataTask = defaultSession.dataTask(with: url) { (data, response, error) in
                if let err = error {
                    if (error as NSError?)?.code != -999 {
                        completion(.failure(err))
                    }
                    return
                }
                if let data = data {
                    if let response = response as? HTTPURLResponse,
                       response.statusCode == 200 {
                        completion(.success(data))
                    } else {
                        completion(.failure(NSError(domain: Constants.statusCodeMessage, code: (response as? HTTPURLResponse)?.statusCode ?? 00, userInfo: nil)))
                    }
                } else {
                    completion(.failure(NSError(domain: Constants.emptyDataMessage, code: 0, userInfo: nil)))
                }
            }
            dataTask?.resume()
        }
    }
}
