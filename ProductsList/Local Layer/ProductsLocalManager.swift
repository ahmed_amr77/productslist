//
//  ProductsLocalManager.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import UIKit
import CoreData

protocol ProductsLocalManagerProtocol {
    func addProduct(product: Product)
    func getAllProducts(completion: ([Product]) -> ())
    func getProduct(with id: Int, completion: (Product?) -> Void)
}

class ProductsLocalManager: ProductsLocalManagerProtocol {
    
    private let appDelegte: AppDelegate?
    private let context: NSManagedObjectContext?
    
    static let sharedInstance = ProductsLocalManager()
    
    private init() {
        appDelegte = UIApplication.shared.delegate as? AppDelegate
        context = appDelegte?.persistentContainer.viewContext
    }
    
    func addProduct(product: Product) {
        guard let context = context else { return }
        checkObject(with: product.id) { (object) in
            if let object = object {
                if let dataObj = object.value(forKey: Constants.productCoreDataKey),
                   let localProduct = Product(dictionary: dataObj as! [String : Any]),
                   product.id == localProduct.id {
                    object.setValue(product.dictionary, forKey: Constants.productCoreDataKey)
                    try? context.save()
                }
            } else {
                let entity = NSEntityDescription.entity(forEntityName: Constants.productEntityName, in: context)
                let mngObj = NSManagedObject(entity: entity!, insertInto: context)
                                
                mngObj.setValue(product.dictionary, forKey: Constants.productCoreDataKey)
                try? context.save()
            }
        }
    }
    
    func getAllProducts(completion: ([Product]) -> ()) {
        var products = [Product]()
        getAllObjects { (objects) in
            guard let objects = objects else { completion(products); return}
            objects.forEach({ (item) in
                if let dict = item.value(forKey: Constants.productCoreDataKey) as? [String : Any],
                   let product =  Product(dictionary: dict){
                        products.append(product)
                }
            })
            completion(products)
        }
    }
    
    func getProduct(with id: Int, completion: (Product?) -> Void) {
        checkObject(with: id) { (object) in
            if let dict = object?.value(forKey: Constants.productCoreDataKey) as? [String : Any],
               let product =  Product(dictionary: dict),
               product.id == id {
                completion(product)
            } else {
                completion(nil)
            }
        }
    }
    
    private func checkObject(with id: Int, completion: (_ object: NSManagedObject?) -> Void) {
        getAllObjects { (objects) in
            guard let objects = objects else { completion(nil); return}
            var isFound = false
            objects.forEach { (object) in
                if let dict = object.value(forKey: Constants.productCoreDataKey) as? [String : Any],
                   let product =  Product(dictionary: dict),
                   product.id == id {
                    completion(object)
                    isFound = true
                    return;
                }
            }
            if !isFound {
                completion(nil)
            }
        }
    }
    
    private func getAllObjects(completion: ([NSManagedObject]?) -> Void) {
        guard let context = context else { return }
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: Constants.productEntityName)
        let mngObj = try? context.fetch(fetchReq)
        completion(mngObj)
    }
}
