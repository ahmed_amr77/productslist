//
//  ProductsPresenter.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import Foundation
import Network

protocol ProductPresenterProtocol {
    func getProducts() -> [Product]
    func getProduct(at index: Int) -> Product
    func fetchProducts()
    func didSelect(at index: Int, imageData: Data?)
}

class ProductPresenter: ProductPresenterProtocol {
   
    private let networkManager: ProductsNetworkkManagerProtocol
    private let localManager: ProductsLocalManagerProtocol
    
    private var productsArr = [Product]()

    private var reachability: Reachability?

    private weak var delegate: ProductsViewProtocol?
    
    init(delegate: ProductsViewProtocol) {
        self.delegate = delegate
        networkManager = ProductsNetworkkManager()
        localManager = ProductsLocalManager.sharedInstance
        initReachability()
    }
    
    private func initReachability() {
        reachability = try! Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability?.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }

    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi, .cellular:
            getProductsFromNetwork()
        case .unavailable:
            getProductsFromLocal()
        }
    }
    
    func getProducts() -> [Product] {
        return productsArr
    }
    
    func getProduct(at index: Int) -> Product {
        return productsArr[index]
    }
    
    func didSelect(at index: Int, imageData: Data?) {
        var product = productsArr[index]
        product.image.data = imageData
        delegate?.performActionWhenItemClick(with: product)
    }
    
    private func monitorConnectivity(completion: @escaping (Bool) -> Void) {
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                completion(true)
            }
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
    
    func fetchProducts() {
        switch reachability?.connection {
        case .wifi, .cellular:
            getProductsFromNetwork()
        case .unavailable:
            getProductsFromLocal()
        case .none:
            break
        }
        
    }
    
    private func getProductsFromNetwork() {
        delegate?.showLoading()
        networkManager.getProductsData(url: Constants.productsURL) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .failure(let err):
                DispatchQueue.main.async {
                    self.delegate?.showAlert(Constants.errorTitle, with: err.localizedDescription, actions: nil)
                }
            case .success(let products):
                DispatchQueue.main.async {
                    self.addToLocal(products: products)
                    self.productsArr += products
                    self.delegate?.renderHome()
                }
            }
            self.delegate?.hideLoading()
        }
    }
    
    private func addToLocal(products: [Product]) {
        products.forEach { (product) in
            localManager.addProduct(product: product)
        }
    }
    
    private func getProductsFromLocal() {
        if productsArr.isEmpty {
            delegate?.showLoading()
            localManager.getAllProducts { (products) in
                self.productsArr += products
                if productsArr.isEmpty {
                    DispatchQueue.main.async {
                        self.delegate?.showAlert(Constants.emptyDataTitle, with: Constants.emptyDataMessage, actions: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.delegate?.renderHome()
                    }
                }
                delegate?.hideLoading()
            }
        }
    }
    
    deinit {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
}
