//
//  Product.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import Foundation

struct Product: Codable {
    let id: Int
    let price: Int
    let productDescription: String
    var image: Image
}

extension Product {
    var dictionary: [String: Any]? {
        guard let jsonData = try? JSONEncoder().encode(self) else { return nil}
        let json = try? JSONSerialization.jsonObject(with: jsonData, options: [])
        guard let dict = json as? [String : Any] else { return nil }
        return dict
    }
    
    init?(dictionary: [String : Any]) {
        do {
            let json = try JSONSerialization.data(withJSONObject: dictionary)
            let decodedProduct = try JSONDecoder().decode(Product.self, from: json)
            self = decodedProduct
        } catch {
            print(error)
            return nil
        }
    }
}

struct Image: Codable {
    let height: Int
    let width: Int
    let url: String
    var data: Data?
}
