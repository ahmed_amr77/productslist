//
//  ProductsViewController.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import UIKit

protocol ProductsViewProtocol: class, BaseViewControllerProtocol {
    func renderHome()
    func performActionWhenItemClick(with product: Product)
}

class ProductsViewController: BaseViewController, UINavigationControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
        
    private var presenter: ProductPresenterProtocol?
    
    private let customAnimation = CustomAnimation()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Products List"
        
        navigationController?.delegate = self

        setCollectionView()
        
        presenter = ProductPresenter(delegate: self)
    }
    
    private func setCollectionView() {
        if let layout = collectionView?.collectionViewLayout as? DynamicHeightLayout {
          layout.delegate = self
        }
        collectionView?.contentInset = UIEdgeInsets(top: 16, left: 8, bottom: 10, right: 8)
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        customAnimation.popStyle = (operation == .pop)
        return customAnimation
    }
}

extension ProductsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.getProducts().count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.productCell, for: indexPath)
        guard let productCell = cell as? ProductCollectionViewCell else { return cell }
        if let product = presenter?.getProduct(at: indexPath.row) {
            productCell.product = product
        }
        return productCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let cell = collectionView.cellForItem(at: indexPath) as? ProductCollectionViewCell
        presenter?.didSelect(at: indexPath.item, imageData: cell?.getImageData())
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        if (indexPaths.last?.item ?? 0) > ((presenter?.getProducts().count ?? 4) - 4) {
            guard presenter != nil else { return }
            presenter!.fetchProducts()
        }
    }
}

extension ProductsViewController: DynamicHeightLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        let cellWidth: CGFloat = (view.bounds.width) / 2
        guard let imageHeight = presenter?.getProduct(at: indexPath.item).image.height,
              let imageWidth = presenter?.getProduct(at: indexPath.item).image.width else { return .zero}
        let imageRatio = CGFloat(imageHeight)/CGFloat(imageWidth)
        return (imageRatio < 1.0 ? 2.0 : imageRatio) * cellWidth
    }
}

extension ProductsViewController: ProductsViewProtocol {
    func renderHome() {
        collectionView.reloadData()
    }
        
    func performActionWhenItemClick(with product: Product) {
        guard let vc = storyboard?.instantiateViewController(identifier: Constants.productDetailsVC, creator: { coder in
            return ProductDetailsViewController(coder: coder, product: product)
        }) else {
            fatalError("Failed to load ProductDetailsViewController from storyboard.")
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
