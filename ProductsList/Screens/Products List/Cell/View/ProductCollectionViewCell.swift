//
//  ProductCollectionViewCell.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import UIKit

protocol ProductCellView: class, BaseViewProtocol {
    func renderViewWith(_ image: UIImage)
    func showError(_ message: String)
}

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var priceLabel: UILabel!
    @IBOutlet weak private var descriptionLabel: UILabel!
    @IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak private var errorLabel: UILabel!
    @IBOutlet weak private var errorLabelContainer: UIView!
    
    private var presenter: ProductCellPresenterProtocol?

    var product: Product? {
        didSet {
            guard let product = product else { return }
            errorLabel.text = ""
            imageView.image = nil
            presenter?.getImage(for: product)
            descriptionLabel.text = product.productDescription
            priceLabel.text = String(product.price) + "$"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureUI()
        
        presenter = ProductCellPresenter(delegate: self)
    }
    
    private func configureUI() {
        self.setBorder(1, with: .darkGray)
        errorLabelContainer.makeRaduis(7)
        errorLabelContainer.setBorder(1, with: .darkGray)
    }
    
    func getImageData() -> Data? {
        return imageView.image?.pngData()
    }
}

extension ProductCollectionViewCell: ProductCellView {
    func showLoading() {
        activityIndicator.startAnimating()
    }
    
    func hideLoading() {
        activityIndicator.stopAnimating()
    }
    
    func renderViewWith(_ image: UIImage) {
        errorLabel.text = ""
        imageView.image = image
    }
    
    func showError(_ message: String) {
        imageView.image = nil
        errorLabel.text = message
    }
}

