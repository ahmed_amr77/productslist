//
//  ProductCellPresenter.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import UIKit

protocol ProductCellPresenterProtocol {
    func getImage(for product: Product)
}

class ProductCellPresenter: ProductCellPresenterProtocol {
    
    private weak var delegate: ProductCellView?
        
    private let networkManager: ProductsNetworkkManagerProtocol
    private let localManager: ProductsLocalManagerProtocol
    private let cacheImages: ImagesCache

    init(delegate: ProductCellView) {
        self.delegate = delegate
        networkManager = ProductsNetworkkManager()
        localManager = ProductsLocalManager.sharedInstance
        cacheImages = ImagesCache.sharedInastance
    }
    
    func getImage(for product: Product) {
        let itemID = NSNumber(value: product.id)
        if let cachedImage = cacheImages.getCachedImage(for: itemID) {
            delegate?.renderViewWith(cachedImage)
        } else {
            getData(for: product)
        }
    }
    
    private func getData(for product: Product) {
        delegate?.showLoading()
        networkManager.getProductImage(url: product.image.url) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .failure(_):
                self.getFromLocal(product)
            case .success(let data):
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    if let image = image  {
                        var newProduct = product
                        newProduct.image.data = data
                        self.cacheImages.setCachedImage(image, for: NSNumber(value: product.id))
                        self.localManager.addProduct(product: newProduct)
                        self.delegate?.renderViewWith(image)
                    } else {
                        if let image = UIImage(named: Constants.placeholderImageName) {
                            self.delegate?.renderViewWith(image)
                        } else {
                            self.delegate?.showError(Constants.imageErrorMessage)
                        }
                    }
                    self.delegate?.hideLoading()
                }
            }
        }
    }
    
    private func getFromLocal(_ product: Product) {
        self.localManager.getProduct(with: product.id) { (localProduct) in
            DispatchQueue.main.async {
                if let localProduct = localProduct,
                   let imageData = localProduct.image.data,
                   let image = UIImage(data: imageData) {
                    self.delegate?.renderViewWith(image)
                } else {
                    let image = UIImage(named: Constants.placeholderImageName)
                    if let image = image {
                        self.delegate?.renderViewWith(image)
                    } else {
                        self.delegate?.showError(Constants.imageErrorMessage)
                    }
                }
                self.delegate?.hideLoading()
            }
        }
    }
}
