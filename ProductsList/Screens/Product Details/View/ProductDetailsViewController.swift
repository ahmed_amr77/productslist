//
//  ProductDetailsViewController.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/6/22.
//

import UIKit

class ProductDetailsViewController: BaseViewController {

    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var priceLabel: UILabel!
    @IBOutlet weak private var descriptionLabel: UILabel!
    
    var product: Product
    
    init?(coder: NSCoder, product: Product) {
        self.product = product
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("You must create this view controller with a product.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI(with: product)
    }
    
    private func setUI(with product: Product) {
        priceLabel.text = String(product.price) + "$"
        descriptionLabel.text = product.productDescription
        if let imageData = product.image.data,
           let image = UIImage(data: imageData) ?? UIImage(named: Constants.placeholderImageName) {
            renderViewWith(image)
        }
    }
    
    private func renderViewWith(_ image: UIImage) {
        imageView.image = image
        guard let color = image.averageColor else { return }
        self.view.backgroundColor = color
        self.priceLabel.textColor = color.isLight ? .black : .white
        self.descriptionLabel.textColor = color.isLight ? .black : .white
    }
}
