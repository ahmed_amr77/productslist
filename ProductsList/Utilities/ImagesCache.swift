//
//  ImagesCache.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/10/22.
//

import UIKit

struct ImagesCache {
    
    private init() {}
    static let sharedInastance = ImagesCache()
    
    private let imagesCache = NSCache<NSNumber, UIImage>()

    func getCachedImage(for id: NSNumber) -> UIImage? {
        return imagesCache.object(forKey: id)
    }
    
    func setCachedImage(_ image: UIImage, for id: NSNumber) {
        imagesCache.setObject(image, forKey: id)
    }
}

