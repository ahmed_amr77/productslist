//
//  Constants.swift
//  ProductsList
//
//  Created by Ahmed Sharf on 2/5/22.
//

import Foundation

struct Constants {
    
    static let statusCodeMessage = "Status code"
    static let emptyDataMessage = "There is no Data !\nPlease check your connection."
    static let emptyDataTitle = "Empty Data"

    static let productCell = "ProductCollectionViewCell"
    
    static let errorTitle = "Error"
    
    static let productsURL = "https://myjson.dit.upm.es/api/bins/d0e9"
    static let placeholderImageName = "placeholder"
    static let imageErrorMessage = "Can't load image !"
    
    static let productEntityName = "ProductData"
    static let productCoreDataKey = "product"
    
    static let productDetailsVC = "ProductDetailsViewController"
}
